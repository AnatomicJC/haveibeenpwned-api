FROM python:3.12.7-alpine3.20 as build
MAINTAINER Jean-Christophe Vassort "anatomicjc@open-web.fr"

# As it is a distroless python container, you will got this output if you launch it:
# docker: Error response from daemon: OCI runtime create failed: container_linux.go:380: starting container process caused: exec: "/bin/sh": stat /bin/sh: no such file or directory: unknown.

# So launch this container like this:
# docker run --rm -p5000:5000 -v "$PWD/pwned.txt:/srv/pwned.txt" --entrypoint /usr/local/bin/python3 haveibeenpwned-api run.py

# You can check if it works by testing this URL http://127.0.0.1:5000/range/37cbd

ARG USER=user
ARG HOME=/home/${USER}

RUN apk upgrade --no-cache; \
    adduser \
    --disabled-password \
    --gecos "" \
    --home ${HOME} \
    ${USER}


COPY --chown=${USER}:${USER} . ${HOME}

USER ${USER}
WORKDIR ${HOME}
ENV PATH=${HOME}/.local/bin:${PATH}

RUN /bin/ash -c "/usr/local/bin/python -m pip install --upgrade pip \
    && python -m pip install -r requirements.txt \
    && find . -name '*pyc' \
    -o -name '*pyo' \
    -o -name '*dist-info' \
    -o -name '*egg-info' \
    -o -name 'tests' \
    -o -name '__pycache__' | xargs rm -rf"

USER root

# Distroless magic, first remove all busybox symlinks
# then use busybox to remove busybox itself and apk

RUN find /sbin /bin /usr/bin -type l  -exec busybox rm -rf {} \;; \
    busybox rm /sbin/apk /bin/busybox

USER ${USER}

ENTRYPOINT /usr/local/bin/python3
CMD run.py
